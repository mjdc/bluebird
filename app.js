var promise = require('bluebird');
var fs = promise.promisifyAll(require('fs'));

inputFile = 'input.txt';
outputFile = 'output.txt'; 

function readAndWrite(){
	fs.readFileAsync(inputFile)
	.then(function(content){
		fs.writeFile(outputFile,content,(err)=>{
			if(err) console.log(err);
			else console.log('succesfull');
		});
	})
	.catch((err)=>{
		console.log(err);
	})
}

readAndWrite();